package com.citi.trading;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;

import java.util.function.Consumer;

import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class InvestorTest {
	
	private Investor investor = new Investor(10000);
	
	
	@Test
	public void testBuy() {
		OrderPlacer market = mock(MockMarket.class);
		
		Mockito.doAnswer(new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				// TODO Auto-generated method stub
				Trade trade = (Trade) invocation.getArguments()[0];
				Consumer<Trade> handler = (Consumer<Trade>) invocation.getArguments()[1];
				handler.accept(trade);
				return "Nico";
			}
			
		}).when(market).placeOrder(any(Trade.class), any(Consumer.class));
		
		investor.setMarket(market);
		investor.buy("GOOG", 1, 5000);
		assertThat(investor.getPortfolio(), hasEntry("GOOG", 1));
		assertThat(investor.getCash(), equalTo(5000.0));
		
	}
	
	@Test
	public void testSell() {
		OrderPlacer market = mock(Market.class);
		
		Mockito.doAnswer(new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				// TODO Auto-generated method stub
				Trade trade = (Trade) invocation.getArguments()[0];
				Consumer<Trade> handler = (Consumer<Trade>) invocation.getArguments()[1];
				handler.accept(trade);
				return "Maki";
			}
			
		}).when(market).placeOrder(any(Trade.class), any(Consumer.class));
		
		investor.setMarket(market);
		investor.buy("GOOG", 1, 5000);
		assertThat(investor.getPortfolio(), hasEntry("GOOG", 1));
		investor.sell("GOOG", 1, 5000);
		//assertThat(investor.getPortfolio(), hasEntry("GOOG", 1));
		assertThat(investor.getCash(), equalTo(10000.0));
		
	}
}
